// NAMA : RAHIMI ILLONG TABAYUNI
// NIM  : 225150707111046

import java.util.LinkedList;
import java.util.Scanner;

class SortedItemList {
    LinkedList<Integer> items;

    public SortedItemList() {
        items = new LinkedList<>();
    }

    // 1. Menambah elemen baru dan langsung mengurutkannya berdasarkan isi list yang
    // ada.
    public void addSort(int angka) {
        int posisi = 0;
        while (posisi < items.size() && posisi < angka) {
            posisi++;
        }
        items.add(posisi, angka);
    }

    // 2. Menghapus elemen dari Linked List berdasarkan nilai yang diberikan.
    public void removeList(int angka) {
        items.remove(Integer.valueOf(angka));
    }

    // 3. Menampilkan seluruh elemen dalam Linked List yang telah diurutkan.
    public void sortAllList() {
        for (int i = 0; i < items.size() - 1; i++) {
            int posisi = i;
            for (int j = i + 1; j < items.size(); j++) {
                if (items.get(j) < items.get(posisi)) {
                    posisi = j;
                }
            }
            int temp = items.get(posisi);
            items.set(posisi, items.get(i));
            items.set(i, temp);
        }
        for (int i = 0; i < items.size(); i++) {
            System.out.print(items.get(i));
            if (i < items.size() - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
    }

    // Method Main
    public static void main(String[] args) {
        SortedItemList items = new SortedItemList();
        Scanner in = new Scanner(System.in);
        int tindakan;
        do {
            System.out.println("Menu: ");
            System.out.println("1. Tambahkan angka");
            System.out.println("2. Tampilkan angka terurut");
            System.out.println("3. Hapus angka");
            System.out.println("4. Keluar");
            System.out.print("Pilih tindakan (1/2/3/4): ");
            tindakan = in.nextInt();
            in.nextLine();
            switch (tindakan) {
                case 1:
                    System.out.print("Masukkan angka: ");
                    int angkaAdd = in.nextInt();
                    items.addSort(angkaAdd);
                    break;
                case 2:
                    System.out.print("Angka yang sudah diurutkan :");
                    items.sortAllList();
                    break;
                case 3:
                    System.out.print("Masukkan angka yang ingin dihapus: ");
                    int angkaRemove = in.nextInt();
                    items.removeList(angkaRemove);
                    break;
                case 4:
                    System.out.println("Terima Kasih!");
                    break;
                default:
                    System.out.println("Pilihan Tindakan Tidak Ada!");
            }
        } while (tindakan != 4);
        in.close();
    }
}